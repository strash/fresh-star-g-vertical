extends Control


const Counter: PackedScene = preload("res://source/scene/Counter.tscn")


signal btn_back_pressed
signal btn_spin_pressed
signal btn_autospin_pressed


# CUSTOMISING
var HEADER_POS: float # header position.y
var BTN_BACK_POS: float # button back position.y
var LEVEL_FG_PROGRESS_WIDTH: float # level progress fg max width
var BET_POS: float # bet position.y
var WIN_POS: float # win score position.y
var MAXBET_POS: float # maxbet position.y
var AUTOSPIN_POS: float # button autospin position.y
var SPIN_POS: float # button spin position.y
var PROGRESS_POS_HIDE: float


# меняется сигналами из Game через Main
var spinning: bool = false
var auto_spinning: bool = false


var SCORE: float = 10000.0 # общий счетчик
var SCORE_WIN = 0.0 # счетчик выигранных очков
var LEVEL: int = 1 # уровень на шкале уровня
var LEVEL_SCORE: float = 0.0 # выигранные очки
var LEVEL_STEP: int = 200 # шаг уровня
const CHANSE_TO_WIN: int = 15 # шанс на победу. условно в процентах
const RATE: float = 15.0 # повышающий коэффициент при выигрыше

const MIN_BET: float = 1.0 # минимальный порог ставки
const MAX_BET: float = 10.0 # максимальный порог ставки
const BET_STEP: float= 1.0 # шаг инкремента/дикремента
var BET: float= 5.0 # текущий коэффициент ставки

var REGEX = RegEx.new()


# BUILTINS - - - - - - - - -


func _ready() -> void:
	set_level(0)
	HEADER_POS = ($Header as Control).get_rect().position.y
	BTN_BACK_POS = ($Header/BtnBack as Button).get_rect().position.y
	LEVEL_FG_PROGRESS_WIDTH = ($Header/Level/ProgressBg/ProgressFg as NinePatchRect).get_rect().size.x
	BET_POS = ($Footer/Bet as Control).get_rect().position.y
	WIN_POS = ($Footer/BgWin as TextureRect).get_rect().position.y
	MAXBET_POS = ($Footer/BtnMaxBet as Button).get_rect().position.y
	AUTOSPIN_POS = ($Footer/BtnAutoSpin as Button).get_rect().position.y
	SPIN_POS = ($Footer/BtnSpin as Button).get_rect().position.y
	PROGRESS_POS_HIDE = ($Footer/AutoSpinProgress as TextureProgress).get_rect().position.y
	REGEX.compile("(\\d)(?=(\\d\\d\\d)+([^\\d]|$))")
	($Footer/AutoSpinProgress as TextureProgress).min_value = 0.0
	($Footer/AutoSpinProgress as TextureProgress).max_value = Global.AUTO_SPIN_MAX
	($Footer/AutoSpinProgress as TextureProgress).step = Global.AUTO_SPIN_MAX / 500.0


func _process(_delta: float) -> void:
	if ($Header/Score/Label as Label).is_visible_in_tree():
		($Header/Score/Label as Label).text = "%s" % format_number(round(SCORE * 100) / 100)
	if ($Header/Level/LevelScoreLabel as Label).is_visible_in_tree():
		($Header/Level/LevelScoreLabel as Label).text = "%s/%s" % [format_number(round(LEVEL_SCORE)), format_number(round(LEVEL * LEVEL_STEP))]
	if ($Footer/BgWin/ScoreWin as Label).is_visible_in_tree():
		($Footer/BgWin/ScoreWin as Label).text = "%s" % format_number(round(SCORE_WIN * 100) / 100)


# METHODS - - - - - - - - -


# отображение контролов
func show_hide_controls(show: bool) -> void:
	var _t: bool
	var btn_back_pos_hide: float = -150.0
	var footer_size: float = ($Footer as Control).get_rect().size.y
	var footer_pos: float = get_rect().size.y - footer_size
	if show:
		_t = ($Tween as Tween).interpolate_property($Header, "rect_position:y", btn_back_pos_hide, HEADER_POS, 0.2)
		_t = ($Tween as Tween).interpolate_property($Header/BtnBack, "rect_position:y", btn_back_pos_hide, BTN_BACK_POS, 0.2)
		_t = ($Tween as Tween).interpolate_property($Header/Level, "modulate:a", 0.0, 1.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Header/Score, "modulate:a", 0.0, 1.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer, "rect_position:y", footer_pos + footer_size + Global.FOOTER_EXTRA_OFFSET, footer_pos, 0.2)
	else:
		_t = ($Tween as Tween).interpolate_property($Header, "rect_position:y", HEADER_POS, btn_back_pos_hide, 0.2)
		_t = ($Tween as Tween).interpolate_property($Header/BtnBack, "rect_position:y", BTN_BACK_POS, btn_back_pos_hide, 0.2)
		_t = ($Tween as Tween).interpolate_property($Header/Level, "modulate:a", 1.0, 0.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Header/Score, "modulate:a", 1.0, 0.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer, "rect_position:y", footer_pos, footer_pos + footer_size + Global.FOOTER_EXTRA_OFFSET, 0.2)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка очков
func set_score() -> void:
	var _t: bool
	var counter: Label = Counter.instance() as Label
	if randi() % 100 <= CHANSE_TO_WIN + CHANSE_TO_WIN * BET / 2:
		var count: int = floor(RATE * BET + LEVEL) as int
		_t = ($Tween as Tween).interpolate_property(self, "SCORE", SCORE, SCORE + count, 0.5)
		_t = ($Tween as Tween).interpolate_property(self, "SCORE_WIN", SCORE_WIN, SCORE_WIN + count, 0.5)
		set_level(count)
		counter.set("increase", true)
		counter.set("count", count)
	else:
		_t = ($Tween as Tween).interpolate_property(self, "SCORE", SCORE, SCORE - BET, 0.5)
		counter.set("increase", false)
		counter.set("count", BET)
	get_parent().add_child(counter)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка подарочных очков
func set_gift_score() -> void:
	var _t: bool
	var counter: Label = Counter.instance() as Label
	_t = ($Tween as Tween).interpolate_property(self, "SCORE", SCORE, SCORE + Global.GIFT_PRICE, 0.5)
	counter.set("increase", true)
	counter.set("count", Global.GIFT_PRICE)
	get_parent().add_child(counter)
	_t = ($Tween as Tween).interpolate_property($Header/BtnGift, "rect_position:y", ($Header/BtnGift as Button).rect_position.y, -200, 0.4, Tween.TRANS_CUBIC)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка уровня
func set_level(count: int) -> void:
	var _t: bool
	LEVEL_SCORE = LEVEL_SCORE + float(count)
	if LEVEL_SCORE > LEVEL * LEVEL_STEP:
		LEVEL_SCORE = LEVEL_SCORE - LEVEL * LEVEL_STEP
		LEVEL += 1
	($Header/Level/LevelLabel as Label).text = "%s" % LEVEL
	var progress: float = lerp($Header/Level/ProgressBg/ProgressFg.rect_min_size.x, LEVEL_FG_PROGRESS_WIDTH, LEVEL_SCORE / (LEVEL * LEVEL_STEP))
	_t = ($Tween as Tween).interpolate_property($Header/Level/ProgressBg/ProgressFg, "rect_size:x", ($Header/Level/ProgressBg/ProgressFg as NinePatchRect).rect_size.x, progress, 0.2)
	_t = ($Tween as Tween).interpolate_property(self, "LEVEL_SCORE", LEVEL_SCORE - count, LEVEL_SCORE, 0.2)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение автоспина
func show_hide_autospin_progress(show: bool) -> void:
	var _t: bool
	var footer_size: float = ($Footer as Control).get_rect().size.y
	var progress_pos_show: float = (footer_size - ($Footer/AutoSpinProgress as TextureProgress).get_rect().size.y) / 2
	if show:
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "rect_position:y", PROGRESS_POS_HIDE, progress_pos_show, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "rect_position:y", BET_POS, BET_POS + footer_size, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BgWin, "rect_position:y", WIN_POS, WIN_POS + footer_size, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "rect_position:y", MAXBET_POS, MAXBET_POS + footer_size, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "rect_position:y", AUTOSPIN_POS, AUTOSPIN_POS + footer_size, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "rect_position:y", SPIN_POS, SPIN_POS + footer_size, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "modulate:a", 0.0, 1.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "modulate:a", 1.0, 0.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "modulate:a", 1.0, 0.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "modulate:a", 1.0, 0.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "modulate:a", 1.0, 0.0, 0.3)
	else:
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "rect_position:y", progress_pos_show, PROGRESS_POS_HIDE, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "rect_position:y", BET_POS + footer_size, BET_POS, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BgWin, "rect_position:y", WIN_POS + footer_size, WIN_POS, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "rect_position:y", MAXBET_POS + footer_size, MAXBET_POS, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "rect_position:y", AUTOSPIN_POS + footer_size, AUTOSPIN_POS, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "rect_position:y", SPIN_POS + footer_size, SPIN_POS, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "modulate:a", 1.0, 0.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "modulate:a", 0.0, 1.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnMaxBet, "modulate:a", 0.0, 1.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "modulate:a", 0.0, 1.0, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "modulate:a", 0.0, 1.0, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($Footer/AutoSpinProgress as TextureProgress).value = 0


# изменеие счетчика количества и прогресса автоспинов
func change_autospin_count() -> void:
	var progress: TextureProgress = ($Footer/AutoSpinProgress as TextureProgress)
	var progress_value: float = progress.value
	var _t: bool = ($Tween as Tween).interpolate_property(progress, "value", progress_value, Global.AUTO_SPIN_COUNT, Global.SPIN_TIME + 1.5)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($Footer/AutoSpinProgress/Label as Label).text = "%s / %s" % [Global.AUTO_SPIN_COUNT, Global.AUTO_SPIN_MAX]


# форматирование числа, проставление знаков между порядками
func format_number(number: float) -> String:
	var formated_number: String = REGEX.sub(String(number), "$1,", true)
	return formated_number


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_back_pressed")


func _on_BtnSpin_button_down() -> void:
	($TimerSpinLongPress as Timer).start()


func _on_BtnSpin_button_up() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_spin_pressed")
		($TimerSpinLongPress as Timer).stop()


func _on_Timer_timeout() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
		change_autospin_count()


func _on_BtnMinus_pressed() -> void:
	var _t: bool
	if not spinning and not auto_spinning:
		BET = BET - BET_STEP if BET > MIN_BET else MIN_BET
		($Footer/Bet/Label as Label).text = str(BET)

		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		if BET == MIN_BET:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 0.0, 0.2)
		else:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.2)

		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnPlus_pressed() -> void:
	var _t: bool
	if not spinning and not auto_spinning:
		BET = BET + BET_STEP if BET < MAX_BET else MAX_BET
		($Footer/Bet/Label as Label).text = str(BET)

		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		if BET == MAX_BET:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, 0.2)
		else:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.2)

		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnAutoSpin_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
		change_autospin_count()


func _on_BtnGift_pressed() -> void:
	set_gift_score()


func _on_BtnMaxBet_pressed() -> void:
	var _t: bool
	if not spinning and not auto_spinning:
		BET = MAX_BET
		($Footer/Bet/Label as Label).text = str(BET)
		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.2)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
