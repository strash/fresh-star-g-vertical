extends Control


signal btn_lvl_pressed


var bg_path: String = "res://source/assets/image/bg_btn_lvl_"
var btn_path: String = "res://source/assets/image/btn_lvl_"

var level: int


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _c: int = ($"." as Control).connect("btn_lvl_pressed", get_parent() as Control, "_on_BigButton_btn_lvl_pressed")
	($"." as Control).hide()
	($"." as Control).modulate.a = 0.0


# METHODS - - - - - - - - -


func show_view(lvl: int) -> void:
	level = lvl
	($BG as TextureRect).texture = load(bg_path + str(lvl) + ".png") as Texture
	($BtnLvl/TextureRect as TextureRect).texture = load(btn_path + str(lvl) + ".png") as Texture
	($"." as Control).show()
	var _t: bool = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 1.0, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func hide_view() -> void:
	var _t: bool = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 0.0, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(0.3), "timeout")
	($"." as Control).hide()


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	hide_view()


func _on_BtnLvl_pressed() -> void:
	hide_view()
	emit_signal("btn_lvl_pressed", level)