extends Node


# GUI
const GIFT_PRICE: int = 500 # gift price
const FOOTER_EXTRA_OFFSET: float = 200.0 # extra offset for hiding footer in "show_hide_controls()"


# GAME
const SPIN_TIME: float = 2.3 # время кручения после разгона
# const STOPPING_SPIN_TIME: float = 1.5 # время остановки кручения
var AUTO_SPIN_COUNT: float = 1.0 # счетчик количества повторов автоспина
const AUTO_SPIN_MAX: float = 10.0 # количество повторов автоспина

const BGS: Array = [
	preload("res://source/assets/image/bg_lvl_1.png"),
	preload("res://source/assets/image/bg_lvl_2.png"),
	preload("res://source/assets/image/bg_lvl_3.png"),
	preload("res://source/assets/image/bg_lvl_4.png"),
]
const BOARDS: Array = [
	preload("res://source/assets/image/board_lvl_1.png"),
	preload("res://source/assets/image/board_lvl_2.png"),
	preload("res://source/assets/image/board_lvl_3.png"),
	preload("res://source/assets/image/board_lvl_2.png"),
]
const SHADOWS: Array = [
	[
#		preload("res://source/assets/image/shadow_top_lvl_1.png"),
#		preload("res://source/assets/image/shadow_bottom_lvl_1.png"),
		null, null,
	],
	[
		null, null,
	],
	[
		null, null,
	],
	[
		null, null,
	],
]

const PROPS: = [
	{
		w = 220,
		h = 211,
		count_x = 3, # количество по горизонтали
		count_y = 3, # количество по вертикали
		offset_x = 13, # отступ между иконками
		offset_y = 0,
		variations = 4, # вариации иконок
		icons_offset_x = 0, # отступ иконок
		icons_offset_y = -120,
		board_bg_offset_x = 0, # отступ картинки доски
		board_bg_offset_y = -126,
		shadow_top_offset_y = 0, # отступ верхней тени
		shadow_bottom_offset_y = 0, # отступ нижней тени
	},
	{
		w = 233,
		h = 214,
		count_x = 3,
		count_y = 3,
		offset_x = 1,
		offset_y = 1,
		variations = 4,
		icons_offset_x = 0,
		icons_offset_y = -120,
		board_bg_offset_x = 0,
		board_bg_offset_y = -121,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
	{
		w = 221,
		h = 224,
		count_x = 3,
		count_y = 3,
		offset_x = 20,
		offset_y = 0,
		variations = 4,
		icons_offset_x = 0,
		icons_offset_y = -120,
		board_bg_offset_x = 0,
		board_bg_offset_y = -120,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
	{
		w = 233,
		h = 214,
		count_x = 3,
		count_y = 3,
		offset_x = 1,
		offset_y = 1,
		variations = 4,
		icons_offset_x = 0,
		icons_offset_y = -120,
		board_bg_offset_x = 0,
		board_bg_offset_y = -121,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
]